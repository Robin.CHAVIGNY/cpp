#include <iostream> 
#include "A.hpp"
#include "B.hpp"

int 
main(int, char **) {
  A ai;
  B bi;

  std::cout << "a a pour valeur " << ai.i << std::endl;
  std::cout << "b a pour valeur " << bi.j << std::endl;

  return 0;
}
