#include <iostream> 
#include "Mere.hpp"

Mere::Mere(){
    x=x+1;
    xpart=x;
    std::cout << "Construction Mere " << x << std::endl;
}
Mere::Mere(std::string newname){
    x=x+1;
    xpart=x;
    nom=newname;
    std::cout << "Construction Mere " << x << std::endl;
} 

Mere::~Mere(){
    std::cout << "Destruction Mere " << xpart << std::endl;
} 

int Mere::getCompteur(){
    return x;
} 

std::string Mere::getName(){
    return nom;
} 

void Mere::afficher(){
    std::cout << "Objet de la classe Mere " << std::endl;
} 