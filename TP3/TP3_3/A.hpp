#ifndef __A_HPP
#define __A_HPP



class B;

class A {
    public: 
        int i;
        void send(B* b); 
        void exec(int entier);
        A();
};

#endif //__A_HPP