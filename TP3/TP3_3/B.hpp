#ifndef __B_HPP
#define __B_HPP

class A;

class B {
    public: 
        int j;
        void send(A* a); 
        void exec(int entier);
        B();
};

#endif //__B_HPP