#include <iostream> 
#include "Mere.hpp"
#include "Fille.hpp"

Fille::Fille():Mere(){
    std::cout << "Construction Fille " << std::endl;
} 
Fille::Fille(std::string newname) : Mere(newname){
    std::cout << "Construction Fille " << std::endl;
} 
Fille::~Fille(){
    std::cout << "Destruction Fille " << std::endl;
} 

void Fille::afficher(){
    std::cout << "Objet de la classe Fille " << std::endl;
}  

