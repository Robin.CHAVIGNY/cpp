#pragma once 


class Point {
    // par défaut, tout est privé dans une "class"  
    int x;
    int y;


    public:
        int getX(); 
        int getY(); 
        void setX(int);
        void setY(int);
        void deplacerDe(int, int);
        void deplacerVers(int, int);
        Point();
        Point(int, int);
        static int compteur;
        static int getCompteur();

};
