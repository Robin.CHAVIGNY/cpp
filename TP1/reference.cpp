#include <iostream>

void fonction1(int a) {
  std::cout << &a << std::endl;
}

void fonction2(int& a) {
  std::cout << &a << std::endl;
}

void echange_pointeur(int *a,int *b){
    int tmp;
    tmp=*a;
    *a=*b;
    *b=tmp;
}  

void echange_reference(int &a,int &b){
    int tmp=a;
    a=b;
    b=tmp;
} 

int main(int, char **) {
  /*int age = 45;

  std::cout << &age << std::endl;
  fonction1(age);
  fonction2(age);

  int  a = 3;
    int  b = a;
    int& c = a;

    std::cout << a << " " << b << " " << c << std::endl;
    b = 4;
    std::cout << a << " " << b << " " << c << std::endl;
    c = 5;
    std::cout << a << " " << b << " " << c << std::endl;
    a = 6;
    std::cout << a << " " << b << " " << c << std::endl;

    a=1;
    b=0;
    std::cout << a << " " << b << std::endl;
    echange_pointeur(&a,&b);
    std::cout << a << " " << b << std::endl;
    echange_reference(a,b);
    std::cout << a << " " << b << std::endl;*/


    const int TAILLE = 500;

   int * p = new int[TAILLE];

   for(auto i = 0; i< TAILLE; ++i ) p[i] = i;
   for(auto i = 0; i< TAILLE; ++i ) std::cout << p[i] << std::endl;

   delete p;      
   //delete [] p;



  return 0;
}

