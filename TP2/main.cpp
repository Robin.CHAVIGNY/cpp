#include <iostream> 
#include "Point.hpp"

int main(int, char**) {
    Point p;
    Point pp(5,5);
    Point * p0 = new Point();
    Point * p1 = new Point(1,1);

    Point p2 {};
    Point p3 {3,3};

    std::cout << p.getX() << std::endl;

    p.setX(51);
    std::cout << p.getX() << std::endl;

    p.setX(1);
    p.setY(2);
    std::cout << p.getX() << " : " << p.getY() << std::endl;

    p.deplacerDe(3,4);
    std::cout << p.getX() << " : " << p.getY() << std::endl;

    p.deplacerVers(1,1);
    std::cout << p.getX() << " : " << p.getY() << std::endl;

    std::cout << Point::getCompteur() << std::endl;
     
    delete p0;
    delete p1;
    return 0;
}
