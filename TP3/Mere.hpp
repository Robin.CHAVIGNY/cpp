#pragma once 
#ifndef BE
#define BE

class Mere {
    protected: 
        inline static int x=0;
        int xpart;
        std::string nom;

    public:
        Mere();
        Mere(std::string);
        ~Mere();  
        int getCompteur();
        std::string getName();
        virtual void afficher();
};

#endif