#include <iostream>  // Inclusion d'un fichier standard
#include "Point.hpp" // Inclusion d'un fichier du répertoire courant

int Point::compteur = 0;

Point::Point() {
    std::cout << "Constructeur sans arguments" << std::endl;
    this->x=0;
    this->y=0;
    this->compteur = this->compteur + 1;
}
Point::Point(int xnew, int ynew) {
    std::cout << "Constructeur avec arguments" << std::endl;
    this->x=xnew;
    this->y=ynew;
    this->compteur = this->compteur + 1;    
}
int Point::getCompteur() {
    return compteur;
}
int Point::getX() {
    return x;
}
int Point::getY() {
    return y;
}
void Point::setX(int new_x) {
    x=new_x;
}
void Point::setY(int new_y) {
    y=new_y;
}
void Point::deplacerDe(int dx, int dy) {
    setX(this->x+dx);
    setY(this->y+dy);
}
void Point::deplacerVers(int xnew, int ynew) {
    setX(xnew);
    setY(ynew);
}