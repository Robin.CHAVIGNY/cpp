#include <iostream> 

class Tableau 
{
    int * tab;
    int taille;

    public:
        void afficher();
        Tableau():tab(nullptr), taille(10) 
        {
            tab = new int[taille]; 
            
            for (int i=0;i<taille; i++){
                tab[i]=i;
            }
        }
        ~Tableau(){
            delete []tab;
        }

};


void Tableau::afficher(){
    for (int i=0; i<this->taille;i++){
        std::cout << this->tab[i] << std::endl;
    }
}

int main(int, char **)
{
   Tableau t;

   t.afficher();

   return 0;
}
